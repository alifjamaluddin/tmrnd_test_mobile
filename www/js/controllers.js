angular.module('starter.controllers', ['ionic', 'ionic-datepicker'])

.controller('DashCtrl', function($ionicPlatform, $scope, $http, ionicDatePicker) {
    baseUrl = "http://rails-tmrnd-test-env.a6m2fwvdcv.ap-southeast-1.elasticbeanstalk.com/"
    var req = {
      method: 'GET',
      url: baseUrl + 'v1/data/top_5',
    }

      $http(req).success(function(response){
        console.log(response)
        $scope.topwebs = response
      })    

      $scope.start_date ="";
      $scope.end_date ="";

  

    var ipObj1 = {
      callback: function (val) {  //Mandatory
        console.log('Return value from the datepicker popup is : ' + val, new Date(val));
        $scope.start_date  = formatDate(new Date(val));

      },
      inputDate: new Date(),      //Optional
      mondayFirst: true,          //Optional
      disableWeekdays: [0],       //Optional
      closeOnSelect: false,       //Optional
      templateType: 'modal'       //Optional
    };

    var ipObj2 = {
      callback: function (val) {  //Mandatory
        console.log('Return value from the datepicker popup is : ' + val, new Date(val));
        $scope.end_date = formatDate(new Date(val));

      },
      inputDate: new Date(),      //Optional
      mondayFirst: true,          //Optional
      disableWeekdays: [0],       //Optional
      closeOnSelect: false,       //Optional
      templateType: 'modal'       //Optional
    };




    $scope.startDatePicker = function(){
      ionicDatePicker.openDatePicker(ipObj1);
    }
    $scope.endDatePicker = function(){
      ionicDatePicker.openDatePicker(ipObj2);
    }

    $scope.updateTopWeb = function(){

        var req1 = {
        method: 'GET',
        url: baseUrl + 'v1/data/top_web_by_date?start_date='+$scope.start_date+'&end_date='+$scope.end_date+'&web_no=5',
        }
        $http(req1).success(function(response){
        console.log(response)
        $scope.topwebs = response
      })    
    }

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

})

// .controller('ChatsCtrl', function($scope, Chats) {
//   // With the new view caching in Ionic, Controllers are only called
//   // when they are recreated or on app start, instead of every page change.
//   // To listen for when this page is active (for example, to refresh data),
//   // listen for the $ionicView.enter event:
//   //
//   //$scope.$on('$ionicView.enter', function(e) {
//   //});

//   $scope.chats = Chats.all();
//   $scope.remove = function(chat) {
//     Chats.remove(chat);
//   };
// })

.controller('WebDetailCtrl', function($scope, $stateParams, $http) {

  baseUrl = "http://rails-tmrnd-test-env.a6m2fwvdcv.ap-southeast-1.elasticbeanstalk.com/"
    var req = {
      method: 'GET',
      url: baseUrl + 'v1/data/web_visitor_hits?domain_name_id='+$stateParams.webId,
    }

    $scope.web_id = $stateParams.webId

      $http(req).success(function(response){
        console.log(response)
        $scope.webs = response
      })    
});

// .controller('AccountCtrl', function($scope) {
//   $scope.settings = {
//     enableFriends: true
//   };
// });


